#pragma once
#include "Objeto.h"
#include "Hueco.h"
#include "Pieza.h"
#include "Jugador.h"
#include "Tablero.h"

class Interaccion
{
public:
	Interaccion(void);
	~Interaccion(void);
	static bool trasladar(Posicion,Objeto*  ,Tablero &); 
	static bool duplicar(Posicion, Objeto*,Tablero &);
	static void contacto(Posicion,Tablero &);
	static bool mover(Posicion, Objeto*,Tablero &); //Esta funcion se encarga de decir si el movimiento deseado implica una traslacion o un duplicado
};

