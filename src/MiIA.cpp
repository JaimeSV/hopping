#include "MiIA.h"
void MiIA::inicializarMatrizDecisiones(){	
		for(int fila=0;fila<FILAMAX;fila++) //Se recorre el tablero inicializando la matriz de decisiones
		{
			for(int columna=0;columna<COLUMNAMAX;columna++)
			{
			matrizMovimientos[fila][columna]=0;
			matrizPiezas[fila][columna]=0;
				for(int f=0;f<FILAMAX;f++)
				{
					for(int c=0;c<COLUMNAMAX;c++)
						matrizDecisiones[fila][columna][f][c]=0;
				}
			}
		}
}
void MiIA::rellenarMatrizMovimientos(){
	inicializarMatrizDecisiones();//Se inicia la matriz de decisiones en 0
	for(int fila=0;fila<FILAMAX;fila++) //Se recorre el tablero en busca de las posibles decisiones
	{
		for(int columna=0;columna<COLUMNAMAX;columna++)
		{
				if(tableroReal->getPieza(Posicion(fila,columna))->getEquipo()==A) //Si la pieza pertenece al IA
				{
					matrizPiezas[fila][columna]=1;
					for(int c=columna-2;c<=columna+2;c++) //Se recorre todos movimientos que puede realizar la IA
					{
						
						for(int f=fila-2;f<=fila+2;f++)
						{
							if(f>=0 && c>=0 && f<FILAMAX && c<COLUMNAMAX)//Si esta dentro del tablero
							{
								if(tableroReal->getPieza(Posicion(f,c))->getEquipo()==N) //Si es un hueco
								{
									matrizMovimientos[f][c]=1; //Puede realizar el movimiento
									Tablero tableroFuturo;		

									//Pieza*	temp =	Pieza(Posicion(fila,columna),A);	//CAMBIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO		
									//if(Interaccion::mover(Posicion(f,c),temp,tableroFuturo))		

									if(Interaccion::mover(Posicion(f,c),&Pieza(Posicion(fila,columna),A),tableroFuturo))
									matrizDecisiones[fila][columna][f][c]=tableroFuturo.cuentaPiezas(A)-tableroFuturo.cuentaPiezas(B); 
								}
							}
						}
					}
			}
		}
	}
} 

Objeto*  MiIA::elegirMovimiento(Posicion& posicion){
	int maximo=0;	
	rellenarMatrizMovimientos();
	Objeto* pieza;
	for(int fila=0;fila<FILAMAX;fila++) //Se recorre el tablero inicializando la matriz de decisiones
		{
			for(int columna=0;columna<COLUMNAMAX;columna++)
			{
				for(int f=0;f<FILAMAX;f++)
				{
					for(int c=0;c<COLUMNAMAX;c++)
					{
						if(matrizDecisiones[fila][columna][f][c]>=maximo)
						{
							maximo=matrizDecisiones[fila][columna][f][c];
							Posicion p(f,c);
							posicion=p;
							pieza=tableroReal->getPieza(Posicion(fila,columna));
						}
					}
				}
			}
		}
	return pieza;
}
			
			
