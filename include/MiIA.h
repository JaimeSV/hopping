#include "Tablero.h"
#include "Interaccion.h"
#pragma once
#define DIMENSIONMAX 36
class MiIA //La IA va a ser el jugador A
{
	Tablero* tableroReal;
//	Mundo* mundoFuturo;
//	Tablero tableroFuturo;
	int matrizPiezas[FILAMAX][COLUMNAMAX];//Se utiliza tan solo como revision
	int matrizMovimientos[FILAMAX][COLUMNAMAX]; //Se utiliza tan solo como revision
	int matrizDecisiones[FILAMAX][COLUMNAMAX][FILAMAX][COLUMNAMAX]; //Matriz de 4 dimensiones->las dos primeras la posicion inicial de la pieza
public:																//Las otras dos la posicion a la que se mueve. El valor de la matriz es la resta entre las piezas de los equipos			

	MiIA() { tableroReal=new Tablero(FILAMAX,COLUMNAMAX);};
	~MiIA(void){ delete tableroReal;}
	void refrescar(Tablero* tab ){
		for(int f=0;f<FILAMAX;f++)
		{
			for(int c=0;c<COLUMNAMAX;c++)
			{
			tableroReal->setPieza(tab->getPieza(Posicion(f,c))); //Constructor copia
			}
		}
	};
	void inicializarMatrizDecisiones();
	void MiniMax();
	void rellenarMatrizMovimientos();
	Objeto * elegirMovimiento(Posicion&);
};