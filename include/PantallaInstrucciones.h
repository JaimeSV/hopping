#include "Pantalla.h"
#pragma once

class PantallaInstrucciones:public Pantalla{  
	static const int anchura=10;
	static const int altura=1;
public:
	virtual void Inicializa(){ //SE HA DECIDIDO REPETIR ESTA FUNCION CON EL OBJETIVO DE QUE LOS OBJETOS SEAN INDEPENDIENTES
		x_ojo=anchura/2; //Nos aseguramos que mire al centro 
		y_ojo=-altura*3/2;
		z_ojo=20;} 
	virtual void Dibuja(){
		gluLookAt(x_ojo, y_ojo, z_ojo,  // posicion del ojo
		anchura/2, (-altura*3)/2, 0,      // hacia que punto mira  ĦĦĦĦĦĦIMPORTANTE MIRA HACIA EL CENTRO  !!!
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    
		glDisable(GL_LIGHTING);
		dibujarInstrucciones();
		glEnable(GL_LIGHTING); 
		}
	virtual void celdaSeleccionada(double x, double y, int& celdaX, int& celdaY){
		if(x<13.8 && x>10 && y>-4.8 && y<-3)celdaY= celdaX=11; //Decido que la celda (11,11) sea la salida
	}
	void dibujarInstrucciones(){
		glColor3ub(150,150,150);
		glBegin(GL_POLYGON);
		glVertex3f(10,-3,-0.01);
		glVertex3f(10,-5,-0.01);
		glVertex3f(14,-5,-0.01);
		glVertex3f(14,-3,-0.01);
		glEnd();
		glColor3ub(255,255,255);
		escribirPorPantalla(10.2,-4,"Volver al menu");
		escribirPorPantalla(x_ojo-4,altura/2+4.5,"INSTRUCCIONES");
		escribirPorPantalla(x_ojo-8,altura/2+3,"Gana aquel jugador que tenga un numero mayor de piezas");
		escribirPorPantalla(x_ojo-8,altura/2+2,"Los movimientos permitidos son:");
		escribirPorPantalla(x_ojo-7,altura/2+1," * Duplicar si se realiza el movimiento a la casilla proxima");
		escribirPorPantalla(x_ojo-7,altura/2," * Trasladar la pieza si el movimiento tiene un alcance de dos celdas");
		escribirPorPantalla(x_ojo-8,altura/2-1,"Cuando se realiza un movimiento y se finaliza en contacto con ");
		escribirPorPantalla(x_ojo-8,altura/2-2,"piezas del otro equipo, estas ultimas cambian de color y pasan");
		escribirPorPantalla(x_ojo-8,altura/2-3,"a formar parte del equipo de la ficha movida.");
	}
};