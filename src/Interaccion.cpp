#include "Interaccion.h"
Interaccion::Interaccion(void)
{
}
Interaccion::~Interaccion(void)
{
}
bool Interaccion::duplicar(Posicion posicion, Objeto *pieza,Tablero &tablero)
{
	if (tablero.insertar(pieza,posicion))//Se manda la pieza que quieres insertar y la posicion. Te devuelve si es posible
		return true;
	else
		return false;		
}

bool Interaccion::trasladar(Posicion posicion, Objeto* pieza,Tablero &tablero)
{
	if( (tablero.insertar(pieza,posicion)))//Se manda la pieza que quieres insertar y la posicion. Te devuelve si es posible
	{
		Hueco huecoNuevo(pieza->getPosicion()); //Al producirse un traslado es necesario insertar un hueco en la posicion de partida
		tablero.insertar(huecoNuevo);
		return true;
	}
	return false;
}

void Interaccion::contacto(Posicion posicionMovida,Tablero &tablero)
{
	Objeto * piezaMovida;
	piezaMovida=tablero.getPieza(Posicion(posicionMovida.fila,posicionMovida.columna));
	for(int f=posicionMovida.fila-1;f<=posicionMovida.fila+1;f++)
	{
		for(int c=posicionMovida.columna-1;c<=posicionMovida.columna+1;c++)
		{
			if(f>=0 && c>=0 && f<FILAMAX && c<COLUMNAMAX){
				if((tablero.getPieza(Posicion(f,c))->getEquipo() ==A)||(tablero.getPieza(Posicion(f,c))->getEquipo() ==B)) //Si existe una pieza se transforma al equipo
				tablero.getPieza(Posicion(f,c))->setEquipo(piezaMovida->getEquipo()); 
			}
		}
	}
}

bool Interaccion::mover(Posicion lugarmovimiento, Objeto *pieza,Tablero &tablero) 
{
		int flag=0; // Si se duplica no mira si se traslada
		for(int c=pieza->getPosicion().columna-1;c<=pieza->getPosicion().columna+1;c++) //Bucle que comprueba si se duplica
		{
			for(int f=pieza->getPosicion().fila-1;f<=pieza->getPosicion().fila+1;f++)
			if(f>=0 && c>=0 && f<FILAMAX && c<COLUMNAMAX){
				if(lugarmovimiento==tablero.getPieza(Posicion(f,c))->getPosicion())
				{
					if(duplicar(lugarmovimiento,pieza,tablero)) //Si se puede duplicar
					{
						contacto(lugarmovimiento,tablero);
						flag=1;
					}
				}
			}
		}
		if(flag==0)
		{
			for(int c=pieza->getPosicion().columna-2;c<=pieza->getPosicion().columna+2;c++) //Bucle que comprueba si se traslada (poco eficiente,pero eficaz)
			{
				for(int f=pieza->getPosicion().fila-2;f<=pieza->getPosicion().fila+2;f++)
					if(f>=0 && c>=0 && f<FILAMAX && c<COLUMNAMAX){
						if(lugarmovimiento==tablero.getPieza(Posicion(f,c))->getPosicion())
						{
							if(trasladar(lugarmovimiento,pieza,tablero)){ //Si se puede trasladar
								contacto(lugarmovimiento,tablero);
								flag=2;
							}
						}
					}
			}
		}
		if(flag==0) return false; //Si no esta dentro de alcance, osea no se a dublicado ni trasladado
		else return true;
}