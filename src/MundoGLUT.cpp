#include "MundoGLUT.h"

void MundoGLUT::Inicializa()
{	
	listaPantallas[Inicial]=new PantallaInicial;
	listaPantallas[Juego]=new PantallaJuego;
	listaPantallas[Final]=new PantallaFinal;
	listaPantallas[Creditos]=new PantallaCreditos;
	listaPantallas[Instrucciones]=new PantallaInstrucciones;
	for(int i=0;i<5;i++)	
		listaPantallas[i]->Inicializa();
	pantallaActual=Inicial;
}
void MundoGLUT::RotarOjo()
{
	//float dist=sqrt(x_ojo*x_ojo+z_ojo*z_ojo);
	//float ang=atan2(z_ojo,x_ojo);
	//ang+=0.05f;
	//x_ojo=dist*cos(ang);
	//z_ojo=dist*sin(ang);
}


void MundoGLUT::Dibuja()
{	
	if((mimundo->getGanador()==A || mimundo->getGanador()==B ||mimundo->tablero.cuentaPiezas(N)==0)&& vertablero==0 )
		pantallaActual=Final;
	listaPantallas[pantallaActual]->refrescar(mimundo);
	listaPantallas[pantallaActual]->Dibuja();
//	ETSIDI::playMusica("sonidos/fondo.mp3",true);
}


void MundoGLUT::Mueve()
{

}
void MundoGLUT::Tecla(unsigned char key)
{
	//float d=sqrt(x_ojo*x_ojo+y_ojo*y_ojo);
	//float theta=atan2(y_ojo,x_ojo);	
	//if (key == 'a')
	//	theta+=0.5;
	//if (key == 'd')
	//	theta-=0.5;
	//x_ojo=d*cos(theta);
	//y_ojo=d*sin(theta);
}


void MundoGLUT::MouseButton(int x,int y,int button,bool down, bool sKey, bool ctrlKey){	
	GLint viewport[4];
    GLdouble modelview[16];
    GLdouble projection[16];
    GLfloat winX, winY, winZ;
    GLdouble posX, posY, posZ;

    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetIntegerv( GL_VIEWPORT, viewport );

    winX = (float)x;
    winY = (float)viewport[3] - (float)y;
    glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
	gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);      
	int f,c;
	listaPantallas[pantallaActual]->celdaSeleccionada(posX,posY,f, c);
	if(pantallaActual==Juego) mimundo->Seleccion(Posicion(f,c));
	if(pantallaActual==Inicial) 
		switch (f)
		{
		case 0: pantallaActual=Juego; mimundo->setNumeroJugadores(false);  break;//Jugar vs IA
		case 1: pantallaActual=Juego;mimundo->setNumeroJugadores(true);break; //Jugar 2 vs 2
		case 2: pantallaActual=Instrucciones;break;
		case 3:	pantallaActual=Creditos; break;
		}
	if(pantallaActual==Creditos)
		if(f==10 && c==10)
			pantallaActual=Inicial;
	if(pantallaActual==Instrucciones)	
		if(f==11 && c==11)
			pantallaActual=Inicial;
	if(pantallaActual==Final)	
		if(f==12 && c==12)
		{
			pantallaActual=Juego;
			vertablero=1;
		}
}



