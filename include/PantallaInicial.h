#include "Pantalla.h"
//#include "ETSIDI.h"
#pragma once

class PantallaInicial:public Pantalla{
	static const int anchura=10;
	static const int altura=1;
public:
	virtual void Inicializa(); 
	virtual void Dibuja(); 
	virtual void celdaSeleccionada(double x, double y, int& celdaX, int& celdaY){
		celdaY= (int)(abs(x/anchura));
		celdaX= (int)(abs(y/(altura*2)));
	}
	void dibujarPantallaInicial();
	void dibujarCreditos();


	long abs (long value) {
	  if (value < 0) {
	    return -value;
	  }
	  else {
	    return value;  
	  }
	}
};
