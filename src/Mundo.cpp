#include "Mundo.h"
Mundo::~Mundo(void)
{
}

bool Mundo::Seleccion(Posicion posicion)
{
	if(posicion==posicionSeleccionada)
		return false;
	if(posicion.fila>=0 && posicion.columna>=0 && posicion.fila<FILAMAX && posicion.columna<COLUMNAMAX){
		seleccionNueva=true;
		posicionSeleccionada=posicion;
		cout<<"celdaX: "<<posicion.fila<<endl;
		cout<<"celdaY: "<<posicion.columna<<endl;
		if(dosjugadores==true)	jugar();
		if(dosjugadores==false){
			jugarIA(); //Se llama dos veces a la IA debido a que una interrupcion por rat�n supone dos movimientos
			jugarIA();// uno del jugador y uno de la IA
		}
		return true;
		}
}
void Mundo::jugar(){
	if(tablero.cuentaPiezas(N)!=0) //Jugar mientras existan huecos
	{
		
		if(marcador==0 && seleccionNueva==true)	//Seleccionar pieza
		{
			piezaSeleccionada= tablero.getPieza(posicionSeleccionada);
			if(piezaSeleccionada->getEquipo()==jugadorA.getEquipo())
			{
			cout<<"Pieza Seleccionada---Posicion: "<<posicionSeleccionada.fila<<" , "<<posicionSeleccionada.columna<<endl;
			seleccionNueva=false;
			marcador=1;
			}
		}
		if((marcador==1)	&& seleccionNueva==true) //Selecciona la posicion a la que se quiere mover
		{
			if(Interaccion::mover(posicionSeleccionada,piezaSeleccionada,tablero))
			{
				marcador=2;
				cout<<"Movimiento realizado"<<endl;
			}
		}
		if(marcador==2 && seleccionNueva==true  )	//Seleccionar pieza
		{
			piezaSeleccionada= tablero.getPieza(posicionSeleccionada);
			if(piezaSeleccionada->getEquipo()==jugadorB.getEquipo())
			{
			cout<<"Pieza Seleccionada---Posicion: "<<posicionSeleccionada.fila<<" , "<<posicionSeleccionada.columna<<endl;
			seleccionNueva=false;
			marcador=3;
			}
		}
		if((marcador==3)	&& seleccionNueva==true) //Selecciona la posicion a la que se quiere mover
		{
			if(Interaccion::mover(posicionSeleccionada,piezaSeleccionada,tablero))
			{
				marcador=0;
				cout<<"Movimiento realizado"<<endl;
			}
		}
		if(tablero.cuentaPiezas(jugadorA.getEquipo())==0)
		{
			ganador=B;
			cout<<"EL JUGADOR AZUL ES EL GANADOR"<<endl;
		}
		if(tablero.cuentaPiezas(jugadorB.getEquipo())==0)
		{
			ganador=A;
		cout<<"EL JUGADOR ROJO ES EL GANADOR"<<endl;
		}
	}
}

void Mundo::jugarIA(){	
	miIA.refrescar(&tablero);
	Posicion posicionSeleccionadaIA;
	if(tablero.cuentaPiezas(N)!=0) //Jugar mientras existan huecos
	{
		if(marcador==0)
		{
			
			piezaSeleccionada=miIA.elegirMovimiento(posicionSeleccionadaIA);
			if(Interaccion::mover(posicionSeleccionadaIA,piezaSeleccionada,tablero))
			{
				marcador=2;
				cout<<"La IA realiza un movimiento desde la posicion ("<<piezaSeleccionada->getPosicion().fila<<" , "<< piezaSeleccionada->getPosicion().fila<<")hacia la posicion("<<posicionSeleccionadaIA.fila <<" , "<<posicionSeleccionadaIA.columna<<") "<<endl;
			}
		}
		if(marcador==2 && seleccionNueva==true  )	//Seleccionar pieza
		{
			piezaSeleccionada= tablero.getPieza(posicionSeleccionada);
			if(piezaSeleccionada->getEquipo()==jugadorB.getEquipo())
			{
			cout<<"Pieza Seleccionada---Posicion: "<<posicionSeleccionada.fila<<" , "<<posicionSeleccionada.columna<<endl;
			seleccionNueva=false;
			marcador=3;
			}
		}
		if((marcador==3)	&& seleccionNueva==true) //Selecciona la posicion a la que se quiere mover
		{
			if(Interaccion::mover(posicionSeleccionada,piezaSeleccionada,tablero))
			{
				marcador=0;
				cout<<"Movimiento realizado"<<endl;
				seleccionNueva=false;
			}
		}
		if(tablero.cuentaPiezas(jugadorA.getEquipo())==0)
		{
			ganador=B;
			cout<<"EL JUGADOR AZUL ES EL GANADOR"<<endl;
		}
		if(tablero.cuentaPiezas(jugadorB.getEquipo())==0)
		{
			ganador=A;
		cout<<"EL JUGADOR ROJO ES EL GANADOR"<<endl;
		}
	}
}