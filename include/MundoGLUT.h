#include <math.h>
#include <sstream>
#include "Mundo.h"
#include "glut.h"
#include "PantallaJuego.h"
#include "PantallaInicial.h"
#include "PantallaCreditos.h"
#include "PantallaFinal.h"
#include "PantallaInstrucciones.h"
//#include "ETSIDI.h"

#pragma once
enum {MOUSE_LEFT_BUTTON, MOUSE_MIDDLE_BUTTON, MOUSE_RIGHT_BUTTON};
enum {KEY_UP, KEY_DOWN, KEY_LEFT,KEY_RIGHT};
enum pantalla_t {Inicial,Juego,Final,Creditos,Instrucciones};

class MundoGLUT
{
	Mundo * mimundo;
	Pantalla *listaPantallas[6];
	pantalla_t pantallaActual;
	bool vertablero; //Indica si al finalizar el juego el jugador quiere ver el tablero
public: 
	MundoGLUT(){vertablero=0;}
	void refrescar(Mundo* mundo){mimundo=mundo;};
	void Tecla(unsigned char key);
	void Inicializa();
	void RotarOjo(); //De momento sin utilidad
	void Mueve(); //De momento sin utilidad
	void Dibuja(); //Se encarga de la coordinación de todos los dibujos
	void dibujarPantallaInicial();
	void MouseButton(int x,int y,int button,bool down,bool shiftKey, bool ctrlKey);
};
