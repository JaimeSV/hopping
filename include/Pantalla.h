#include <string.h>
#include <sstream>
#include "glut.h"
#include "Mundo.h"
#pragma once
#define WIDHT 2
class Pantalla{ //Clase virtual pura
protected:
	Mundo * mimundo;
	float x_ojo;
	float y_ojo;
	float z_ojo;
public:
	virtual void Inicializa()=0; 
	virtual void Dibuja()=0;
	virtual void celdaSeleccionada(double x, double y, int& celdaX, int& celdaY)=0;
	void refrescar(Mundo* mundo){mimundo=mundo;};
	void escribirPorPantalla(float x,float y,char* str)
	{
		int len, i=5;
		glRasterPos2f(x, y);
		len = (int) strlen(str);
		for (i = 0; i < len; i++) {
			 //glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, str[i]);	//OTRO TIPO DE LETRA
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, str[i]);	
		}
	}
};