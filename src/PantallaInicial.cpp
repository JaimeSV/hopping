#include "PantallaInicial.h"

void PantallaInicial::Inicializa()
{
	x_ojo=anchura/2; //Nos aseguramos que mire al centro 
	y_ojo=-altura*3/2;
	z_ojo=20;
//	ETSIDI::playMusica("sonidos/fondo.mp30",true);
}
void PantallaInicial::Dibuja()
{
	gluLookAt(x_ojo, y_ojo, z_ojo,  // posicion del ojo
		anchura/2, (-altura*3)/2, 0,      // hacia que punto mira  ĦĦĦĦĦĦIMPORTANTE MIRA HACIA EL CENTRO  !!!
	0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    
	glDisable(GL_LIGHTING);
	dibujarPantallaInicial(); 
	glEnable(GL_LIGHTING);

}
void PantallaInicial::dibujarPantallaInicial(){	
	char* mensaje[4];
	mensaje[0]="Un jugador";
	mensaje[1]="Dos jugadores";
	mensaje[2]="Instrucciones";
	mensaje[3]="Creditos";
	for(int i=0;i<8;i+=2){
		glColor3ub(150,150,150);
		glBegin(GL_POLYGON);
		glVertex3f(x_ojo+anchura/2,-i,-0.01);
		glVertex3f(x_ojo-anchura/2,-i,-0.01);
		glVertex3f(x_ojo-anchura/2,-altura-i,-0.01);
		glVertex3f(x_ojo+anchura/2,-altura-i,-0.01);
		glEnd();
		glColor3ub(250,250,250);
		escribirPorPantalla(x_ojo-1,altura/2-i-0.5,mensaje[i/2]);
	}	
}

