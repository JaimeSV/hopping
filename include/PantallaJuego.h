#include "Pantalla.h"

#pragma once

class PantallaJuego:public Pantalla
{
	static const int anchura=WIDHT;
	static const int altura=WIDHT;
public:
	virtual void Inicializa(); 
	virtual void Dibuja(); 
	virtual void celdaSeleccionada(double x, double y, int& celdaX, int& celdaY){
		celdaX= (int)(abs(y/altura));
		celdaY= (int)(x/anchura);
	}
	void dibujarCuadrado(int, int,int,int);
	void dibujarPieza(int, int,int,int); //Dibuja cada pieza individualmente
	void dibujarPiezas(); //Dibuja todas las piezas	
	void imprimirInformacion();
	void dibujarTablero();
	void dibujarBordeTablero();


	long abs(long value) {
  	if (value < 0) {
   	 return -value;
	 	 }
	 	 else {
	   	 return value;  
	 	 }
	}
};