#include "Tablero.h"
#include "Jugador.h"
#include "Interaccion.h"
#include "MiIA.h"
using namespace std;
#include <iostream>
#pragma once
class Mundo
{
	bool seleccionNueva;
	Posicion posicionSeleccionada;
	int marcador;
	Objeto* piezaSeleccionada;
	Equipo ganador;
	bool dosjugadores; //Si esta true juegan dos jugadores si es false 1 vs IA
public:
	Mundo(void): jugadorA(A),jugadorB(B),seleccionNueva(true),marcador(0),posicionSeleccionada(Posicion(3,3)),ganador(N){} ;//HAY QUE HACER LIMPIEZA
	~Mundo(void);
	Tablero tablero;
	Jugador jugadorA ;//(Equipo A);
	Jugador jugadorB;//(Equipo B);
	MiIA miIA;
	bool Seleccion(Posicion);
	void jugar();
	void jugarIA();
	void setNumeroJugadores(bool n){dosjugadores=n;};
	Equipo getGanador(){return ganador;};
	Equipo getTurno(){	
	if(marcador==0 || marcador== 1)
		return A;
		else 
			return B;
	}
};

