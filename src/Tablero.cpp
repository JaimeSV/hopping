#include "Tablero.h"


Tablero::Tablero(void)
{
	for(int f=0;f<fMAX;f++)
	{
		for(int c=0;c<cMAX;c++)
		{
			tablero[f][c]= new Hueco(Posicion(f,c)); //Inicializamos todos los espacios del tablero con huecos
		}
	}
	tablero[0][0]=new Pieza(Posicion(0,0),A);
	tablero[fMAX-1][cMAX-1]=new Pieza(Posicion((fMAX-1),(cMAX-1)),B);
}


Tablero::~Tablero(void)
{
	for(int f=0;f<fMAX;f++)
	{
		for(int c=0;c<fMAX;c++)
		{
			delete tablero[f][c]; //Eliminamos de la reserva de memoria todos los objetos
		}
	}
}

bool Tablero::insertar(Objeto* pieza,Posicion posicion)
{
	if((posicion.columna<cMAX)&&(posicion.fila<fMAX)) //Esta dentro de los limites del tablero
		if((tablero[posicion.fila][posicion.columna]->getEquipo()==N)) //Hay un hueco en el lugar de la inserción
		{
			delete tablero[posicion.fila][posicion.columna]; //Borramos la pieza anterior
			Pieza piezaNueva(posicion,pieza->getEquipo());
			tablero[posicion.fila][posicion.columna]=new Pieza(piezaNueva); //Insertamos la nueva pieza
			return true;
		}
	return false;
}
bool Tablero::insertar(Hueco h)
{
	if((h.getPosicion().columna<cMAX)&&(h.getPosicion().fila<fMAX)) //Esta dentro de los limites del tablero
	{	
		delete tablero[h.getPosicion().fila][h.getPosicion().columna]; //Borramos la pieza anterior
		tablero[h.getPosicion().fila][h.getPosicion().columna]=new Hueco (h); //Insertamos el nuevo hueco
		return true;
	}
	return false;
}
int Tablero::cuentaPiezas(Equipo e){
	int numpiezas=0;
	for(int f=0;f<fMAX;f++)
	{
		for(int c=0;c<cMAX;c++)
		{
			if(tablero[f][c]->getEquipo()==e)
				numpiezas++; 
		}
	}
	return numpiezas;
}