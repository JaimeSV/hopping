#include "PantallaJuego.h"

void PantallaJuego::Inicializa()
{
	x_ojo=COLUMNAMAX*anchura/2; //Nos aseguramos que mire al centro del tablero
	y_ojo=-FILAMAX*altura/2;
	z_ojo=20;
}
void PantallaJuego::Dibuja()
{
	gluLookAt(x_ojo, y_ojo, z_ojo,  // posicion del ojo
	COLUMNAMAX*anchura/2, -FILAMAX*altura/2, 0.0,      // hacia que punto mira  ĦĦĦĦĦĦIMPORTANTE MIRA HACIA EL CENTRO DEL TABLERO !!!
	0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    
	glDisable(GL_LIGHTING);
	dibujarPiezas();
	dibujarTablero();
	dibujarBordeTablero();
	imprimirInformacion();
	glEnable(GL_LIGHTING);
}
void PantallaJuego::dibujarBordeTablero(){ 
	if(mimundo->getTurno()==A) //El color del borde del tablero indica que jugador tiene realizar el movimiento
		glColor3ub(250,0,0);
	else
		glColor3ub(0,0,255);
	glBegin(GL_POLYGON); 
	glVertex3f(-0.5,0.5,0);
	glVertex3f(-0.5,-FILAMAX*anchura-0.5,0);
	glVertex3f(COLUMNAMAX*altura+0.5,-FILAMAX*anchura-0.5,0);	
	glVertex3f(COLUMNAMAX*altura+0.5,0.5,0);
	glEnd();
}
void PantallaJuego::dibujarCuadrado(int f, int c,int anchura,int altura){
	glBegin(GL_POLYGON);
	glVertex3f(c*anchura,-f*altura,0);
	glVertex3f((c+1)*anchura,-f*altura,0);
	glVertex3f((c+1)*anchura,-(f+1)*altura,0);
	glVertex3f(c*anchura,-(f+1)*altura,0);
	glEnd();	
}
void PantallaJuego::dibujarPieza(int f,int c,int anchura,int altura){
	glPushMatrix();
	glTranslated(c*anchura+1,-f*altura-1,0);
	glScalef(1, 1, 0.0);
	glRotatef(60,0.1,0.1,0.0);
	glutWireIcosahedron();
	glTranslated(-c*anchura+1,f*altura-1,0);
	glPopMatrix();
}
void PantallaJuego::dibujarTablero(){ 
	for(int f=0;f<FILAMAX;f++)
	{
		for(int c=0;c<COLUMNAMAX;c++){
		if(((c+f)%2)==0) //Para hacer que cambie de color, uno si-uno no
			glColor3ub(150,150,150);
		else 
			glColor3ub(255,255,255);
		dibujarCuadrado(f,c,anchura,altura); 	//Dibujamos los cuadrados 
		}
	}
}
void PantallaJuego::dibujarPiezas(){
	for(int f=0;f<FILAMAX;f++)
	{
		for(int c=0;c<COLUMNAMAX;c++)
		{
			if(mimundo->tablero.getPieza(Posicion(f,c))->getEquipo()==A) //Buscamos una pieza del equipo A
			{
				glColor3ub(250,0,0);
				dibujarPieza(f,c,anchura,altura);
			}
			if(mimundo->tablero.getPieza(Posicion(f,c))->getEquipo()==B)
			{
				glColor3ub(0,0,255);
				dibujarPieza(f,c,anchura,altura);
			}
		}
	}
}

void PantallaJuego::imprimirInformacion(){
	glColor3ub(150,150,150);
	//Datos del jugador A
	string piezasA="";
	piezasA = std::to_string( mimundo->tablero.cuentaPiezas(A));
	string mensajeA = "Piezas del jugador rojo: "+piezasA;
	char* smsA=new char[mensajeA.size()];
	strcpy(smsA,mensajeA.c_str());
	escribirPorPantalla(0, 1.5, smsA);
	//Datos del jugador B
	string piezasB="";
	piezasB = std::to_string( mimundo->tablero.cuentaPiezas(B));
	string mensajeB = "Piezas del jugador azul: "+piezasB;
	char* smsB=new char[mensajeB.size()];
	strcpy(smsB,mensajeB.c_str());
	escribirPorPantalla(0, 1, smsB);
}

