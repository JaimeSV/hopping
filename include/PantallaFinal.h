#include "Pantalla.h"
#pragma once

class PantallaFinal:public Pantalla{  
	static const int anchura=10;
	static const int altura=1;
public:
	virtual void Inicializa(){ //SE HA DECIDIDO REPETIR ESTA FUNCION CON EL OBJETIVO DE QUE LOS OBJETOS SEAN INDEPENDIENTES
		x_ojo=anchura/2; //Nos aseguramos que mire al centro 
		y_ojo=-altura*3/2;
		z_ojo=20;} 
	virtual void Dibuja(){
		gluLookAt(x_ojo, y_ojo, z_ojo,  // posicion del ojo
		anchura/2, (-altura*3)/2, 0,      // hacia que punto mira  ĦĦĦĦĦĦIMPORTANTE MIRA HACIA EL CENTRO  !!!
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    
		glDisable(GL_LIGHTING);
		dibujarFinal();
		glEnable(GL_LIGHTING); 
		}
	virtual void celdaSeleccionada(double x, double y, int& celdaX, int& celdaY){
		if(x<13.8 && x>10 && y>-4.8 && y<-3)celdaY= celdaX=12; //Decido que la celda (12,12) sea la salida
	}
	void dibujarFinal(){
		if (mimundo->tablero.cuentaPiezas(A)>mimundo->tablero.cuentaPiezas(B))
		{
			glColor3ub(250,0,0);
			escribirPorPantalla(x_ojo-5,altura/2-0.5,"YA TENEMOS GANADOR");
			escribirPorPantalla(x_ojo-5,altura/2-1.5,"ENHORABUENA JUGADOR ROJO ");
		}
		if (mimundo->tablero.cuentaPiezas(A)<mimundo->tablero.cuentaPiezas(B))
		{
			glColor3ub(0,0,250);
			escribirPorPantalla(x_ojo-5,altura/2-0.5,"YA TENEMOS GANADOR");
			escribirPorPantalla(x_ojo-5,altura/2-1.5,"ENHORABUENA JUGADOR AZUL");
		}
		if(mimundo->tablero.cuentaPiezas(A)==mimundo->tablero.cuentaPiezas(B))
		{
			glColor3ub(0,250,0);
			escribirPorPantalla(x_ojo-7,altura/2-0.5,"ERA DIFICIL PERO LO HABEIS CONSEGUIDO");
			escribirPorPantalla(x_ojo-5,altura/2-1.5," EMPATE!");
		}
		glColor3ub(150,150,150);
		glBegin(GL_POLYGON);
		glVertex3f(10,-3,-0.01);
		glVertex3f(10,-5,-0.01);
		glVertex3f(14,-5,-0.01);
		glVertex3f(14,-3,-0.01);
		glEnd();
		glColor3ub(255,255,255);
		escribirPorPantalla(10.2,-4,"Volver al tablero");
	}
};