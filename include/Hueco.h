#pragma once
#include "Objeto.h"
class Hueco :public Objeto
{
	const Equipo equipo;
public:
	Hueco(Posicion p):Objeto(p),equipo(N){};
	virtual Equipo getEquipo(){return equipo;};
	virtual	void setEquipo(Equipo e){};
};

