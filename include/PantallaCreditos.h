#include "Pantalla.h"
#pragma once

class PantallaCreditos:public Pantalla{
	static const int anchura=10;
	static const int altura=1;
public:
	virtual void Inicializa(){
		x_ojo=anchura/2; //Nos aseguramos que mire al centro 
		y_ojo=-altura*3/2;
		z_ojo=20;} 
	virtual void Dibuja(){
		gluLookAt(x_ojo, y_ojo, z_ojo,  // posicion del ojo
		anchura/2, (-altura*3)/2, 0,      // hacia que punto mira  ĦĦĦĦĦĦIMPORTANTE MIRA HACIA EL CENTRO  !!!
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    
		glDisable(GL_LIGHTING);
		dibujarCreditos();
		glEnable(GL_LIGHTING); 
		}
	virtual void celdaSeleccionada(double x, double y, int& celdaX, int& celdaY){
		if(x<13.8 && x>10 && y>-4.8 && y<-3)celdaY= celdaX=10; //Decido que la celda (10,10) sea la salida
	}
	void dibujarCreditos(){	
		glColor3ub(150,150,150);
		glBegin(GL_POLYGON);
		glVertex3f(10,-3,-0.01);
		glVertex3f(10,-5,-0.01);
		glVertex3f(14,-5,-0.01);
		glVertex3f(14,-3,-0.01);
		glEnd();
		glColor3ub(255,255,255);
		escribirPorPantalla(10.2,-4,"Volver al menu");
		escribirPorPantalla(x_ojo-3,altura/2+4.5,"CREDITOS");
		escribirPorPantalla(x_ojo-4,altura/2+2,"Jaime Simarro Viana");
		escribirPorPantalla(x_ojo-4,altura/2+1,"Guillermo Ruiz Herrero");
	}
};